const robin = require('roundrobin');
var iterations = 0;
var results = [];
var strategies = ['ALL-D', 'RANDOM', 'TIT-FOR-TAT', 'TESTER', 'JOSS', 'PRAKAC'];

$(function() {
    loadPicker();
    $('#play').click(function() {
        results = [];
        for (var j = 1; j <= strategies.length; j++) {
            for (
                var i = 0;
                i < document.getElementById(`slider${j}`).value;
                i++
            ) {
                var e = document.getElementById(`strat${j}`);
                results.push({
                    id: results.length + 1,
                    strategy: e.innerHTML,
                    totalScore: 0,
                });
            }
        }

        iterations = document.getElementById('number-input').value;

        var elimination = document.getElementById('eliminationCB');
        if (elimination.checked == true) {
            var elim = [];
            while (results.length > 1) {
                for (var j = 0; j < results.length; j++) {
                    results[j].totalScore = 0;
                    results[j].score = 0;
                }
                var schedule = robin(results.length, results);
                results = tournament(schedule);

                results = results.sort((x, y) => x.totalScore - y.totalScore);

                if (
                    results
                        .map(x => x.totalScore)
                        .every((val, i, arr) => val === arr[0])
                ) {
                    break;
                }
                //console.log(results);
                displayResults(results);
                console.log(
                    results[results.length - 1].strategy +
                        ' ' +
                        results[results.length - 1].id
                );
                results[results.length - 1].eliminated = true;
                elim.push(results.pop());
            }
            displayResults(elim.concat(results));
        } else {
            var schedule = robin(results.length, results);
            results = tournament(schedule);
            displayResults(results);
        }
    });
});

function tournament(schedule) {
    schedule.forEach(round => {
        round.forEach(match => {
            var player1 = match[0];
            var player2 = match[1];

            player1.score = 0;
            player2.score = 0;
            player1.betrayed = false;
            player2.betrayed = false;

            const playerHistory1 = [];
            const playerHistory2 = [];

            for (i = 0; i < iterations; i++) {
                player1 = play(player1, playerHistory1, playerHistory2);
                player2 = play(player2, playerHistory2, playerHistory1);

                playerHistory1.push(player1.choice);
                playerHistory2.push(player2.choice);

                /*
                        True=(cooperate)
                        False=(defect)
                                 |  P2 True  |  P2 False
                        ---------|-----------|-----------
                        P1 True  |   Both +1 |   P1 +3
                        ---------|-----------|-----------
                        P1 False |   P2 +3   |   Both +2
                    */

                if (player1.choice && player2.choice) {
                    player1.score += 1;
                    player2.score += 1;
                } else if (player1.choice && !player2.choice) {
                    player1.score += 3;
                    player2.score += 0;
                } else if (!player1.choice && player2.choice) {
                    player1.score += 0;
                    player2.score += 3;
                } else if (!player1.choice && !player2.choice) {
                    player1.score += 2;
                    player2.score += 2;
                }
            }

            var foundIndex = results.findIndex(x => x.id == player1.id);
            results[foundIndex].totalScore += player1.score;

            var foundIndex = results.findIndex(x => x.id == player2.id);
            results[foundIndex].totalScore += player2.score;
        });
    });

    return results;
}

function shuffle(array) {
    var currentIndex = array.length,
        temporaryValue,
        randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    return array;
}

setTimeout(function() {
    //do what you need here
}, 2000);

function displayResults(results) {
    results = results.sort((x, y) => x.totalScore - y.totalScore);
    var html = '';
    results.forEach(function(e, i) {
        html += `<tr><td>${
            e.eliminated ? `Eliminated round #${results.length-i} ${e.strategy}` : e.strategy
        }</td><td>${e.totalScore}</td></tr>`;
    });
    document.getElementById('putHere').innerHTML = html;
}

function loadPicker() {
    var html = '';
    var counter = 1;
    strategies.forEach(e => {
        html += `
        <div class="dropdown row">
            <label class="col-4" id="strat${counter}">${e}</label>
            <div class="slidecontainer col-8">
                <input
                    type="range"
                    min="0"
                    max="10"
                    value="1"
                    class="slider"
                    id="slider${counter}"
                />
                <div id="sliderVal${counter}"></div>
            </div>
        </div>`;
        counter += 1;
    });
    document.getElementById('picker').innerHTML = html;

    for (var i = 1; i <= strategies.length; i++) {
        $(`#slider${i}`).on('input change', generate_handler(i));
    }
    for (var i = 1; i <= strategies.length; i++) {
        const val = document.getElementById(`slider${i}`).value;
        document.getElementById('sliderVal' + i).innerHTML = val;
    }
}

function updateSlider(slideAmount, i) {
    document.getElementById('sliderVal' + i).innerHTML = slideAmount;
}

function generate_handler(i) {
    return function(event) {
        updateSlider(event.target.value, i);
    };
}

function play(player, myHistory, opponentHistory) {
    switch (player.strategy) {
        case 'RANDOM':
            player = stratRandom(player);
            break;
        case 'ALL-D':
            player = stratAllD(player);
            break;
        case 'TIT-FOR-TAT':
            player = stratTitForTat(player, opponentHistory);
            break;
        case 'TESTER':
            player = stratTester(player, myHistory, opponentHistory);
            break;
        case 'JOSS':
            player = stratJoss(player, opponentHistory);
            break;
        case 'PRAKAC':
            player = statPraKac(player, opponentHistory);
            break;
    }
    return player;
}

function stratRandom(player) {
    if (Math.random() >= 0.5) {
        player.choice = true;
    } else {
        player.choice = false;
    }
    return player;
}

function stratTitForTat(player, opponentHistory) {
    if (opponentHistory.length == 0) {
        player.choice = true;
    } else {
        player.choice = opponentHistory[opponentHistory.length - 1];
    }
    return player;
}

function stratAllD(player) {
    player.choice = false;
    return player;
}

function stratTester(player, myHistory, opponentHistory) {
    if (myHistory.length == 0) {
        player.choice = false;
    } else if (
        player.betrayed ||
        (myHistory.length > 1 &&
            myHistory[myHistory.length - 2] == false &&
            opponentHistory[opponentHistory.length - 1] == false)
    ) {
        player.betrayed = true;
        player = stratTitForTat(player, opponentHistory);
    } else if (myHistory[myHistory.length - 1] == false) {
        player.choice = true;
    } else {
        player.choice = false;
    }
    return player;
}

function stratJoss(player, opponentHistory) {
    if (opponentHistory.length == 0) {
        player.choice = true;
    } else {
        player.choice = !opponentHistory[opponentHistory.length - 1];
    }

    if (player.choice && Math.random() >= 0.9) {
        player.choice = false;
    }

    return player;
}

function statPraKac(player, opponentHistory) {
    if (opponentHistory.length == 0) {
        player.choice = false;
    } else if (
        opponentHistory[opponentHistory.length - 1] == false ||
        opponentHistory[opponentHistory.length - 2] == false
    ) {
        player.choice = false;
    } else {
        player.choice = true;
    }

    return player;
}
